# Django-Books

Django application where logged in users can view and add book reviews. A user should also be able to delete his/her own reviews.

![](http://s3.amazonaws.com/General_V88/boomyeah/company_209/chapter_3834/handouts/chapter3834_6666_sample1-books.png)
