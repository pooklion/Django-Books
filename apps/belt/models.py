from __future__ import unicode_literals

from django.db import models
import re
import bcrypt
import datetime

EMAIL_REGEX = re.compile(r'^[a-zA-Z0-9.+_-]+@[a-zA-Z0-9._-]+\.[a-zA-Z]+$')

class UserManager(models.Manager):
    def regis_validate(self, post_data):
        result = {
            'status' : 'pass',
            'data' : ''
        }
        error = []
        # check length of name
        if len(post_data['name']) < 2:
            error.append('Name field required at least 2 characters.')
        # validate email
        if not EMAIL_REGEX.match(post_data['email']):
            error.append('Invalid Email Address.')
        # password length not < 8    
        if len(post_data['password']) < 8:
            error.append('Password required 8 or more characters.')
        # check if passwords matched
        if not post_data['password'] == post_data['c_password']:
            error.append('Passwords not matched.')
        if len(error) > 0:
            result = {
                'status' : 'fail',
                'data' : error
            }
        else:
            hashed_pw = bcrypt.hashpw(post_data['password'].encode(), bcrypt.gensalt())
            user = User.objects.create(name=post_data['name'], alias=post_data['alias'], email=post_data['email'], password=hashed_pw)
            result['data'] = user
        return result
    
    def login_validate(self, post_data):
        result = {
            'status' : 'pass',
            'data' : ''
        }
        # check if email in db
        try: 
            user = User.objects.get(email = post_data['email'])
        except:
            result['status'] = 'fail'
            result['data'] = 'Email or Password incorrect.'
            return result
        # check if password correct
        if bcrypt.checkpw(post_data['password'].encode(), user.password.encode()):
            result['data'] = user
            return result
        else:
            result['status'] = 'fail'
            result['data'] = 'Email or Password incorrect.'
            return result


class User(models.Model):
    name = models.CharField(max_length = 255)
    alias = models.CharField(max_length = 255)
    email = models.CharField(max_length = 255)
    password = models.CharField(max_length = 255)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    objects = UserManager()

class Author(models.Model):
    name = models.CharField(max_length = 255)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

class Book(models.Model):
    title = models.CharField(max_length = 255)
    author = models.ForeignKey (Author, related_name = 'books')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

class Review(models.Model):
    review = models.TextField()
    rating = models.IntegerField()
    book = models.ForeignKey (Book, related_name = 'reviews')
    reviewer = models.ForeignKey (User, related_name = 'reviews')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

