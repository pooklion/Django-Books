from __future__ import unicode_literals
from django.shortcuts import render,redirect
from django.contrib import messages
from models import *
import re
import bcrypt
import datetime

EMAIL_REGEX = re.compile(r'^[a-zA-Z0-9.+_-]+@[a-zA-Z0-9._-]+\.[a-zA-Z]+$')

def index(request):
    return render(request, 'belt/index.html')

def register(request):
    validate = User.objects.regis_validate(request.POST)
    if validate['status'] == 'fail':
        for each in validate['data']:
            messages.error(request,each)
        return redirect('/')     
    else:
        request.session['user_id'] = validate['data'].id
        return redirect('/success')

def login(request):
    validate = User.objects.login_validate(request.POST)
    if validate['status'] == 'fail':
        messages.error(request,validate['data'])
        return redirect('/')
    else:
        request.session['user_id'] = validate['data'].id
        return redirect('/success')
        

def success(request):
    if 'user_id' in request.session: 
        recent_review = Review.objects.all().order_by('-created_at')[:3:1]
        context = {
            'user' : User.objects.get(id = request.session['user_id']),
            'recent_review' : recent_review,
            'other_books' : Book.objects.exclude(reviews__in=recent_review),
        }
        return render(request, 'belt/home.html', context)
    else:
        return redirect('/')

def new_book(request):
    context = {
        'author' : Author.objects.all().order_by('name')
    }
    return render(request, 'belt/upload_book.html', context)
def new_review(request,book_id):
    this_book = Book.objects.get(id = book_id)
    context = {
        'reviews' : Review.objects.filter(book = this_book).order_by('-created_at'),
        'book' : this_book,
        'user_id' : request.session['user_id']
    }
    return render(request, 'belt/upload_review.html', context)

def upload(request,book_id):
    # new book or exist book
    if book_id == '0':
        # if 'New Author' 
        if len(request.POST['new_author']) > 0:
            this_author = Author.objects.create(name = request.POST['new_author'])
            print 'author created'
        else: # exist Author
            this_author = Author.objects.get(id = request.POST['author'])
        this_book = Book.objects.create(title = request.POST['title'], author = this_author)
        
    else:
        this_book = Book.objects.get(id = int(book_id))
    # add review
    user = User.objects.get(id = request.session['user_id'])
    review = Review.objects.create(review = request.POST['review'], rating = request.POST['rating'], book = this_book, reviewer = user)

    return redirect('/success')

def display_user(request, reviewer_id):
    this_user = User.objects.get(id = reviewer_id)
    
    context = {
        'user' : this_user,
        'total' : this_user.reviews.all().count(),
        'books' : Book.objects.filter(reviews__reviewer__id = reviewer_id).distinct()
    }
    return render(request, 'belt/user.html', context)

def delete(request,review_id):
    Review.objects.get(id = review_id).delete()
    return redirect('/success')

def logout(request):
    request.session.flush()
    return redirect('/')
