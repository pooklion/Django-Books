#Get the html
import urllib2
response = urllib2.urlopen('http://en.wikipedia.org/wiki/List_of_programming_languages')
html = response.read()

#Parse it with beautifulsoup
from bs4 import BeautifulSoup
soup = BeautifulSoup(html)

langs = []

#Parse all the links.
for link in soup.find_all('a'):
    #Last link after ZPL, the last language.
    if link.get_text() == u'Top':
        break
    if link.get_text() == u'edit':
        pass
    else:
        langs.append(link.get_text())

# find u'See also'
see_also_index_ = langs.index(u'See also')
# strip out headers
langs = langs[see_also_index_+1:]

print langs