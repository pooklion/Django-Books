from django.conf.urls import url
from . import views 

urlpatterns = [
    url(r'^$', views.index),
    url(r'^register$', views.register),
    url(r'^login$', views.login),
    url(r'^success$', views.success),
    url(r'^new_book$', views.new_book),
    url(r'^new_review/(?P<book_id>\d+)$', views.new_review),
    url(r'^upload/(?P<book_id>\w+)$', views.upload),
    url(r'^display_user/(?P<reviewer_id>\d+)$', views.display_user),
    url(r'^delete/(?P<review_id>\d+)$', views.delete),
    url(r'^logout$', views.logout),
]